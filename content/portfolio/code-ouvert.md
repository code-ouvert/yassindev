---
title: Code Ouvert
date: 2020-08-22T18:19:35.000+06:00
thumbnail: images/portfolio/code-ouvert.png
service: Création de site internet (Hugo)
client: Code Ouvert
link: https://code-ouvert.gitlab.io/code-ouvert/
shortDescription: Code Ouvert Web est un blog de codage où des artlcles liés à HTML, CSS, JavaScript, PHP… ainsi que des framework tels que symfony, laravel… sont publiees. Les codes sources de chaque vidéos YouTube sont fornies et vous pouvez utiliser ces codes sans aucune restriction ni limitation.
challenge: Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
  eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua
  vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren.
solution: Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
  eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua
  vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren.

---
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

Ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.